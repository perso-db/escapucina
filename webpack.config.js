require("webpack");
const path = require('path');

module.exports = {
  entry: "./src/index.js",
  resolve: {
    root: [path.resolve(__dirname, './src')],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: "@svgr/webpack",
            options: { svgoConfig: { plugins: [{ cleanupIDs: false, },], }, },
          },
        ],
      },
    ],
  },
};

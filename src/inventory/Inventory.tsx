import { useAppDispatch, useAppSelector } from "../app/hooks";
import { RootState } from "../app/store";
import "./Inventory.css";
import { familyToElement } from "../items/ItemOpened";
import {
  openFamily,
  selectFamily,
  unselectFamily,
  getItemsInventory,
  getFamilySelected,
  getFamilyOpened,
} from "../items/itemsSlice";

export default function Inventory() {
  const dispatch = useAppDispatch();

  const columnsQuantity = 2;
  const rowsQuantity = 6;

  const inventoryContent = useAppSelector((state: RootState) =>
    getItemsInventory(state)
  );

  const itemSelected = useAppSelector((state: RootState) =>
    getFamilySelected(state)
  );

  const itemOpened = useAppSelector((state: RootState) => getFamilyOpened(state));

  function itemClass(family: string | undefined) {
    if (family === undefined) {
      return "";
    }
    if (family === itemOpened) {
      return "itemOpened";
    } else if (family === itemSelected) {
      return "itemSelected";
    } else {
      return "itemUnselected";
    }
  }

  function generateRows() {
    let rowsArray: Array<JSX.Element> = [];
    let inventoryToDisplay = Object.entries(inventoryContent);
    for (let row = 1; row <= rowsQuantity; row++) {
      rowsArray.push(
        <tr key={"row" + String(row)}>
          {generateColumns(row, inventoryToDisplay)}
        </tr>
      );
    };
    return rowsArray;
  }

  function generateColumns(row: number, inventoryToDisplay: Array<[string, Array<number>]>) {
    let columnsArray: Array<JSX.Element> = [];
    for (let column = 1; column <= columnsQuantity; column++) {
      let familyAndIndexes = inventoryToDisplay.pop();
      if (familyAndIndexes !== undefined) {
        let [family, indexes] = familyAndIndexes;
        let element = familyToElement[family];
        let quantity = indexes.length;
        if (element !== undefined) {
          columnsArray.push(
            <td
              key={"cell" + String(row) + String(column)}
              id={"cell" + String(row) + String(column)}
              onClick={() => onItemClick(family)}
              className={itemClass(family)}
            >
              {element !== undefined ? element : ""}
              {quantity > 1 ? <p className="quantity">{quantity}</p> : ""}
            </td>
          );

        }
      } else {
        columnsArray.push(
          <td
            key={"cell" + String(row) + String(column)}
            id={"cell" + String(row) + String(column)}
          >
          </td>
        )
      }
    }
    return columnsArray;
  }

  function onItemClick(family: string | undefined) {
    if (family !== undefined) {
      if (family !== itemOpened) {
        dispatch(selectFamily(family));
      }
    } else {
      dispatch(unselectFamily());
    }
  }

  function openSelectedItem() {
    if (itemSelected !== undefined) {
      dispatch(openFamily(itemSelected));
      dispatch(unselectFamily());
    }
  }

  return (
    <div className="Inventory">
      <div>
        <table>
          <tbody>{generateRows()}</tbody>
        </table>
      </div>
      <button onClick={openSelectedItem} className="OpenItemButton">
        Ouvrir{" "}
      </button>
    </div>
  );
}

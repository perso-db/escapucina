import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export const bandeIds = [
  "bande1",
  "bande2",
  "bande3",
  "bande4",
  "bande5",
  "bande6",
  "bande7",
] as const;

export type BandeId = typeof bandeIds[number];

interface BandelettesState {
  bandesPlaced: Array<BandeId>;
  isFinished: boolean;
}

const initialState: BandelettesState = {
  bandesPlaced: [],
  isFinished: false,
};

const bandelettesSlice = createSlice({
  name: "bandelettes",
  initialState,
  reducers: {
    placeBande(state, action: PayloadAction<BandeId>) {
      state.bandesPlaced.push(action.payload);
    },
    finish(state) {
      state.isFinished = true;
    },
  },
});

export const { placeBande, finish } = bandelettesSlice.actions;

export const getBandesPlaced = (state: RootState) => {
  return state.bandelettes.bandesPlaced;
};

export const isFinished = (state: RootState) => {
  return state.bandelettes.isFinished;
};

export default bandelettesSlice.reducer;

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface LockState {
    positions: Array<number>;
    isFinished: boolean;
}

const initialState: LockState = {
    positions: [0, 0, 0, 0],
    isFinished: false,
};

const lockSlice = createSlice({
    name: "lock",
    initialState,
    reducers: {
        modifyPositions(state, action: PayloadAction<Array<number>>) {
            state.positions = action.payload;
        },
        finish(state) {
            state.isFinished = true;
        },
    },
});

export const { modifyPositions, finish } = lockSlice.actions;

export const getPositions = (state: RootState) => {
    return state.lock.positions;
};

export const isFinished = (state: RootState) => {
    return state.lock.isFinished;
};

export default lockSlice.reducer;

import { useEffect, useCallback } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { RootState } from "../../app/store";

import {
    placeBande,
    finish,
    getBandesPlaced,
    bandeIds,
    BandeId,
    isFinished,
} from "./bandelettesSlice";
import {
    getFamilySelected,
    removeItemFromInventory,
    unselectFamily,
    getItemsInventory,
} from "../../items/itemsSlice";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";

import { ReactComponent as BandelettesSVG } from "./svgs/Bandelettes.svg";
import RoomId from "../roomId";

export default function Bandelettes() {
    const dispatch = useAppDispatch();
    const familySelected = useAppSelector((state: RootState) =>
        getFamilySelected(state)
    );
    const bandesPlaced = useAppSelector((state: RootState) =>
        getBandesPlaced(state)
    );
    const areAllBandePlaced = useAppSelector((state: RootState) =>
        isFinished(state)
    );
    const inventoryContent = useAppSelector((state: RootState) => getItemsInventory(state));

    const displayBandesPlaced = useCallback(() => {
        for (const bandeId of bandeIds) {
            const bandeElement = document.getElementById(bandeId)!;
            if (bandesPlaced.includes(bandeId)) {
                bandeElement.style.display = "block";
            } else {
                bandeElement.style.display = "none";
            }
        }
    }, [bandesPlaced]);

    const placeBandeOnPanel = () => {
        if (familySelected) {
            var firstItemIndex = inventoryContent[familySelected].pop()!;
            var bandeId = familySelected.toString() + firstItemIndex.toString();
            if ((bandeIds as readonly string[]).includes(bandeId)) {
                dispatch(placeBande(bandeId as BandeId));
                dispatch(removeItemFromInventory([familySelected, firstItemIndex]));
                dispatch(unselectFamily());
                if (bandesPlaced.length === 6) { dispatch(finish()) }
            }
        }
    };

    useEffect(() => {
        displayBandesPlaced();
        document.getElementById("toFirstFloorFaceDown")!.onclick = () => {
            dispatch(setCurrentRoom(RoomId.FirstFloorFaceDown));
        };
    }, [dispatch, displayBandesPlaced]);

    useEffect(() => {
        if (areAllBandePlaced) {
            dispatch(setCurrentSentences(["Tout m'a l'air en place, mais qu'est-ce que ca peut bien vouloir dire..."]));
        } else {
            dispatch(
                setCurrentSentences(["Hum, il semble manquer quelque chose"])
            );
        }
    }, [dispatch, areAllBandePlaced]);

    return (
        <BandelettesSVG onClick={placeBandeOnPanel} className="RoomSVG" />
    );
}

import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { RootState } from "../../app/store";

import {
    modifyPositions,
    finish,
    getPositions,
    isFinished,
} from "./lockSlice";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";

import { ReactComponent as LockSVG } from "./svgs/Lock.svg";
import RoomId from "../roomId";

export default function ElectricalPanel() {
    const dispatch = useAppDispatch();
    const positions = useAppSelector((state: RootState) =>
        getPositions(state)
    );
    const lists = [
        ["red1", "green1", "purple1", "brown1"],
        ["yellow2", "purple2"],
        ["A", "B", "C", "D"],
        ["G", "A", "B", "T", "R", "H", "I"]
    ];
    const isLockFinished = useAppSelector((state: RootState) =>
        isFinished(state)
    );

    const displayLock = () => {
        displayElement(0);
        displayElement(1);
        displaySymbol(2, "text3");
        displaySymbol(3, "text4");
    };

    function displaySymbol(index: number, textBoxName: string) {
        var list = lists[index];
        var symbolIndex = positions[index];
        var symbol = list[symbolIndex];
        document.getElementById(textBoxName)!.textContent = symbol;
    };

    function displayElement(index: number) {
        var elements = lists[index];
        for (var element of elements) {
            document.getElementById(element)!.style.display = 'None';
        }
        var elementIndex = positions[index];
        var elementDisplayed = elements[elementIndex];
        document.getElementById(elementDisplayed)!.style.display = 'block';
    }

    const onToLivingRoomFaceUpClick = () => {
        dispatch(setCurrentRoom(RoomId.LivingRoomFaceUp));
    };

    const updatePositions = (new_positions: Array<number>) => {
        dispatch(modifyPositions(new_positions))
        if (JSON.stringify(new_positions) === JSON.stringify([1, 1, 1, 4])) {
            dispatch(finish())
        }
    }

    const nextPosition = (index: number) => {
        var max_position = lists[index].length;
        var new_position = (positions[index] + 1) % max_position;
        var new_positions = [...positions];
        new_positions[index] = new_position;
        updatePositions(new_positions)
    }

    const previousPosition = (index: number) => {
        var max_position = lists[index].length;
        var new_position = (positions[index] + max_position - 1) % max_position;
        var new_positions = [...positions];
        new_positions[index] = new_position;
        updatePositions(new_positions)
    }

    const connectButtons = () => {
        document.getElementById("plus0")!.onclick = () => nextPosition(0);
        document.getElementById("minus0")!.onclick = () => previousPosition(0);
        document.getElementById("plus1")!.onclick = () => nextPosition(1);
        document.getElementById("minus1")!.onclick = () => previousPosition(1);
        document.getElementById("plus2")!.onclick = () => nextPosition(2);
        document.getElementById("minus2")!.onclick = () => previousPosition(2);
        document.getElementById("plus3")!.onclick = () => nextPosition(3);
        document.getElementById("minus3")!.onclick = () => previousPosition(3);
    }

    useEffect(() => {
        document.getElementById("toLivingRoomFaceUp")!.onclick = onToLivingRoomFaceUpClick;
        if (isLockFinished) {
            dispatch(setCurrentSentences(["C'est ouvert ! Felicitations"]));
        } else {
            dispatch(
                setCurrentSentences([
                    "Un cadenas de demo, truc de fou",
                    "La comme ca, je diras vert violet B R",
                    "Mais bon, ca veut peut-etre rien dire...",
                    "Je dis ca, je dis rien..."
                ])
            );
        }
        connectButtons();
        displayLock()
    });

    return (
        <LockSVG className="RoomSVG" />
    );
}

import { useEffect, useState, useCallback } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { RootState } from "../../app/store";

import {
  isReadyToPlay,
  placeFuse,
  finish,
  getFusesPlaced,
  fuseIds,
  FuseId,
} from "./electricalPanelSlice";
import {
  closeFamily,
  getFamilySelected,
  removeItemFromInventory,
  unselectFamily,
  getItemsInventory,
} from "../../items/itemsSlice";
import { setCurrentRoom, turnTheLightsOn } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";

import { ReactComponent as ElectricalPanelSVG } from "./svgs/ElectricalPanel.svg";
import RoomId from "../roomId";

export default function ElectricalPanel() {
  const dispatch = useAppDispatch();
  const familySelected = useAppSelector((state: RootState) =>
    getFamilySelected(state)
  );
  const fusesPlaced = useAppSelector((state: RootState) =>
    getFusesPlaced(state)
  );
  const isPanelReady = useAppSelector((state: RootState) =>
    isReadyToPlay(state)
  );
  const inventoryContent = useAppSelector((state: RootState) => getItemsInventory(state));
  const [fusesOn, setFusesOn] = useState([] as Array<FuseId>);
  interface FusesModified {
    fuse1: Array<FuseId>;
    fuse2: Array<FuseId>;
    fuse3: Array<FuseId>;
    fuse4: Array<FuseId>;
    fuse5: Array<FuseId>;
    fuse6: Array<FuseId>;
  }

  const displayFusesPlaced = useCallback(() => {
    for (const fuseId of fusesPlaced) {
      const fuseElementId = fuseId + "Off";
      const fuseElement = document.getElementById(fuseElementId)!;
      fuseElement.style.display = "block";
    }
  }, [fusesPlaced]);

  const toggleFuses = useCallback(
    (fusesToModify: Array<FuseId>) => {
      let newFusesOn = fusesOn.slice();
      for (const fuseId of fusesToModify) {
        toggleFuse(fuseId);
        if (newFusesOn.includes(fuseId)) {
          newFusesOn = newFusesOn.filter((id) => id !== fuseId);
        } else {
          newFusesOn.push(fuseId);
        }
      }
      setFusesOn(newFusesOn);
      if (newFusesOn.length === fuseIds.length) {
        dispatch(finish());
        dispatch(closeFamily());
        dispatch(turnTheLightsOn());
        dispatch(setCurrentSentences(["Et la lumiere fut !"]));
      }
    },
    [dispatch, fusesOn]
  );

  const onButtonClick = useCallback(
    (event: Event) => {
      if (isPanelReady) {
        const fusesModified: FusesModified = {
          fuse1: ["fuse2"],
          fuse2: ["fuse1", "fuse3"],
          fuse3: ["fuse2", "fuse4"],
          fuse4: ["fuse3", "fuse5"],
          fuse5: ["fuse4", "fuse6"],
          fuse6: ["fuse5"],
        };
        var elementId = (event.target as SVGPathElement).id;
        var fuseId = elementId.substring(0, 5) as FuseId;
        var fusesToModify = fusesModified[fuseId];
        toggleFuses(fusesToModify);
      }
    },
    [isPanelReady, toggleFuses]
  );

  const toggleFuse = (fuseId: string) => {
    var fuseOnId = fuseId + "On";
    const fuseElement = document.getElementById(fuseOnId)!;
    const visibility = fuseElement.style.display;
    fuseElement.style.display = visibility === "block" ? "none" : "block";
  };

  const placeButtonOnPanel = () => {
    if (familySelected) {
      let indexes = inventoryContent[familySelected];
      var firstItemIndex = indexes[0];
      if (firstItemIndex !== undefined) {
        var fuseId = "fuse" + firstItemIndex.toString();
        if ((fuseIds as readonly string[]).includes(fuseId)) {
          dispatch(placeFuse(fuseId as FuseId));
          dispatch(removeItemFromInventory([familySelected, firstItemIndex]));
          dispatch(unselectFamily());
        }
      }
    }
  };

  const connectButtons = useCallback(() => {
    for (let fuseId of fuseIds) {
      var elementIdOn = fuseId + "On";
      document.getElementById(elementIdOn)!.onclick = onButtonClick;
      var elementIdOff = fuseId + "Off";
      document.getElementById(elementIdOff)!.onclick = onButtonClick;
    }
  }, [onButtonClick]);

  useEffect(() => {
    displayFusesPlaced();
    connectButtons();
  }, [connectButtons, dispatch, displayFusesPlaced, isPanelReady]);

  useEffect(() => {
    if (isPanelReady) {
      dispatch(setCurrentSentences(["C'est bon, ca devrait marcher !"]));
    } else {
      dispatch(
        setCurrentSentences(["OK, quelqu'un a enleve les fusibles, ils ne doivent pas etre bien loin"])
      );
    }
    document.getElementById("toHallFaceUp")!.onclick = () => {
      dispatch(setCurrentRoom(RoomId.HallFaceUp));
    };
  }, [dispatch, isPanelReady]);

  return (
    <ElectricalPanelSVG onClick={placeButtonOnPanel} className="RoomSVG" />
  );
}

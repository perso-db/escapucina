import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export const fuseIds = [
  "fuse1",
  "fuse2",
  "fuse3",
  "fuse4",
  "fuse5",
  "fuse6",
] as const;

export type FuseId = typeof fuseIds[number];

interface electricalPanelState {
  fusesPlaced: Array<FuseId>;
  isFinished: boolean;
}

const initialState: electricalPanelState = {
  fusesPlaced: ["fuse5", "fuse6"],
  isFinished: false,
};

const electricalPanelSlice = createSlice({
  name: "electricalPanel",
  initialState,
  reducers: {
    placeFuse(state, action: PayloadAction<FuseId>) {
      state.fusesPlaced.push(action.payload);
    },
    finish(state) {
      state.isFinished = true;
    },
  },
});

export const { placeFuse, finish } = electricalPanelSlice.actions;

export const isReadyToPlay = (state: RootState) => {
  return state.electricalPanel.fusesPlaced.length === 6;
};

export const getFusesPlaced = (state: RootState) => {
  return state.electricalPanel.fusesPlaced;
};

export const isFinished = (state: RootState) => {
  return state.electricalPanel.isFinished;
};

export default electricalPanelSlice.reducer;

import { useAppSelector } from "../app/hooks";
import { getCurrentRoomId } from "./roomsSlice";
import RoomId from "./roomId";

import HallFaceUp from "./rooms/HallFaceUp";
import HallFaceDown from "./rooms/HallFaceDown";
import PoolFaceLeft from "./rooms/PoolFaceLeft";
import PoolFaceDown from "./rooms/PoolFaceDown";
import LivingRoomFaceUp from "./rooms/LivingRoomFaceUp";
import LivingRoomFaceRight from "./rooms/LivingRoomFaceRight";
import StairsFaceUp from "./rooms/StairsFaceUp";
import KitchenFaceLeft from "./rooms/KitchenFaceLeft";
import KitchenFaceDown from "./rooms/KitchenFaceDown";
import Drawers from "./rooms/Drawers";
import Frigogidaire from "./rooms/Frigogidaire";
import DiningRoomFaceUp from "./rooms/DiningRoomFaceUp";
import FirstFloorFaceDown from "./rooms/FirstFloorFaceDown";
import RoomRom1 from "./rooms/RoomRom1";
import RoomMart1 from "./rooms/RoomMart1";
import RoomTiph1 from "./rooms/RoomTiph1";
import RoomGui1 from "./rooms/RoomGui1";

import ElectricalPanel from "./miniGames/ElectricalPanel";
import Bandelettes from "./miniGames/Bandelettes";
import Lock from "./miniGames/Lock";

import "./Rooms.css";

type RoomsDict = {
  [key in RoomId]: JSX.Element;
};

const roomsDict: RoomsDict = {
  // RDC New Names
  [RoomId.HallFaceUp]: <HallFaceUp />,
  [RoomId.HallFaceDown]: <HallFaceDown />,
  [RoomId.LivingRoomFaceUp]: <LivingRoomFaceUp />,
  [RoomId.LivingRoomFaceRight]: <LivingRoomFaceRight />,
  [RoomId.PoolFaceLeft]: <PoolFaceLeft />,
  [RoomId.PoolFaceDown]: <PoolFaceDown />,
  [RoomId.StairsFaceUp]: <StairsFaceUp />,
  [RoomId.KitchenFaceLeft]: <KitchenFaceLeft />,
  [RoomId.KitchenFaceDown]: <KitchenFaceDown />,
  [RoomId.Drawers]: <Drawers />,
  [RoomId.Frigogidaire]: <Frigogidaire />,
  [RoomId.DiningRoomFaceUp]: <DiningRoomFaceUp />,

  //First Floor New Names
  [RoomId.FirstFloorFaceDown]: <FirstFloorFaceDown />,
  [RoomId.RoomRom1]: <RoomRom1 />,
  [RoomId.RoomTiph1]: <RoomTiph1 />,
  [RoomId.RoomMart1]: <RoomMart1 />,
  [RoomId.RoomGui1]: <RoomGui1 />,

  //Mini games
  [RoomId.ElectricalPanel]: <ElectricalPanel />,
  [RoomId.Bandelettes]: <Bandelettes />,
  [RoomId.Lock]: <Lock />,
};

export default function CurrentRoom() {
  const currentRoomId = useAppSelector(getCurrentRoomId);
  const isLightOn = useAppSelector((state) => state.rooms.isLightOn);

  return (
    <div className="Room">
      {roomsDict[currentRoomId]}
      <div className={isLightOn ? "" : "blackVeil"}></div>
    </div>
  );
}

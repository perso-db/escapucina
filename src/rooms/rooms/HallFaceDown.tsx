import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { useEffect } from "react";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import RoomId from "../roomId";
import { ReactComponent as HallFaceDownSVG } from "./svgs/HallFaceDown.svg";

export default function HallFaceDown() {
  const dispatch = useAppDispatch();

  const onToPoolFaceLeftClick = () => {
    dispatch(setCurrentRoom(RoomId.PoolFaceLeft));
  };

  const onToHallFaceUpClick = () => {
    dispatch(setCurrentRoom(RoomId.HallFaceUp));
  };

  const isFinalDoorHandleOn = useAppSelector(
    (state) => state.rooms.isFinalDoorHandleOn
  );
  const onExitDoorClick = () => {
    if (!isFinalDoorHandleOn) {
      dispatch(
        setCurrentSentences([
          "La porte s'est fermée toute seule, c'est pas rassurant, ça.",
          "On dirait que la poignée a été retirée de ce côté. Impossible de sortir",
          "Elle doit être quelque-part dans la maison..."
        ])
      );
    } else {
      dispatch(
        setCurrentSentences(["Ca fonctionne ! Par ici la sortie !!"])
      );
    }
  };

  const onTreeOfHatClick = () => {
    dispatch(setCurrentSentences([
      "On dirait que ces chapeaux sont là depuis une éternité. Intrigant…",
      "J'en prendrais bien un mais on n'est pas le 2 mai, ça porte malheur.",
      "Ah tiens, une bandelette de papier ?", /*que dans le premier texte*/
    ]));
  };

  useEffect(() => {
    document.getElementById("toPoolFaceLeft")!.onclick = onToPoolFaceLeftClick;
    document.getElementById("toHallFaceUp")!.onclick = onToHallFaceUpClick;
    document.getElementById("exitDoor")!.onclick = onExitDoorClick;
    document.getElementById("TreeOfHat")!.onclick = onTreeOfHatClick;
    dispatch(setCurrentSentences([
      "On voit la sortie pourtant !",
      "Il me faut cette poignée.",
    ]));
  });

  return <HallFaceDownSVG className="RoomSVG" />;
}

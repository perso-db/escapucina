import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { ReactComponent as KitchenFaceDownSvg } from "./svgs/KitchenFaceDown.svg";
import { useEffect, useState } from "react";
import { setCurrentRoom, openDrawers } from "../roomsSlice";
import { getFamilySelected, removeItemFromInventory } from "../../items/itemsSlice";

import RoomId from "../roomId";
import { setCurrentSentences } from "../../narration/narrationSlice";
import { RootState } from "../../app/store";

export default function KitchenFaceDown() {
  const dispatch = useAppDispatch();
  const isKeySelected = useAppSelector((state: RootState) => getFamilySelected(state) === "Key");
  const areDrawersOpen = useAppSelector((state: RootState) => state.rooms.areDrawersOpen);

  const onToDiningRoomFaceUpClick = () => {
    dispatch(setCurrentRoom(RoomId.DiningRoomFaceUp));
  };

  const onToStairsFaceUpClick = () => {
    dispatch(setCurrentRoom(RoomId.StairsFaceUp));
  };

  const onShelf1Click = () => {
    dispatch(setCurrentSentences(["On peut faire périmer du sel ??"]));
  };
  const onShelf2Click = () => {
    dispatch(setCurrentSentences([
      "La Plancha pour les nuls. Avec post-it et annotations.",
      "Quelqu'un a barré \"les nuls\"."
  ]));
  };
  const onDoorClick = () => {
    dispatch(setCurrentSentences(["On dirait pas mais elle est fermée à clé. Clairement."]));
  };

  const onDrawerClick = () => {
    dispatch(setCurrentSentences(["Des tupperwares sous des tupperwares, et entre d'autres tupperwares."]));
  };

  const onToDrawersClick = () => {
    if (areDrawersOpen) {
      dispatch(setCurrentRoom(RoomId.Drawers));
    } else if (isKeySelected) {
      dispatch(setCurrentRoom(RoomId.Drawers));
      dispatch(removeItemFromInventory(["Key", 1]));
      dispatch(openDrawers());
    } else {
      console.debug("close")
      dispatch(setCurrentSentences([
        "C'est fermé à clé.",
        "Elle ne doit pas être bien loin."
    ]))
    }
  };

  useEffect(() => {
    document.getElementById("Shelf1")!.onclick = onShelf1Click;
    document.getElementById("Shelf2")!.onclick = onShelf2Click;
    document.getElementById("Door")!.onclick = onDoorClick;
    document.getElementById("Drawer")!.onclick = onDrawerClick;
    document.getElementById("toStairsFaceUp")!.onclick = onToStairsFaceUpClick;
    document.getElementById("toDiningRoomFaceUp")!.onclick =
      onToDiningRoomFaceUpClick;
    document.getElementById("toDrawers")!.onclick = onToDrawersClick;
    dispatch(setCurrentSentences(["Une vielle cuisine abandonnée... Tout a l'air un tout petit peu pourri.."]));
  });

  return <KitchenFaceDownSvg className="RoomSVG" />;
}

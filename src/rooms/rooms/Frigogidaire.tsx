
import { useAppDispatch } from "../../app/hooks";
import { useEffect } from "react";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import RoomId from "../roomId";
import { ReactComponent as FrigogidaireSVG } from "./svgs/Frigogidaire.svg";

export default function Frigogidaire() {
  const dispatch = useAppDispatch();

  function onToKitchenFaceLeftClick() {
    dispatch(setCurrentRoom(RoomId.KitchenFaceLeft));
  }

  const onShelf1Click = () => {
    dispatch(setCurrentSentences(["Qui range sa vaisselle dans un frigogidaire ?"]));
  };
  const onShelf2Click = () => {
    dispatch(setCurrentSentences(["Ca ne se mange plus."]));
  };
  const onShelf3Click = () => {
    dispatch(setCurrentSentences(["C'est périmé. Hmmm... Encore une bandelette."]));
  };

  useEffect(() => {
    document.getElementById("toKitchenFaceLeft")!.onclick = onToKitchenFaceLeftClick;
    document.getElementById("Shelf1")!.onclick =
      onShelf1Click;
    document.getElementById("Shelf2")!.onclick =
      onShelf2Click;
    document.getElementById("Shelf3")!.onclick = onShelf3Click;
    dispatch(
      setCurrentSentences(["Est-ce un frigo ? Est-ce un frigidaire ?"])
    );
  });

  return <FrigogidaireSVG className="RoomSVG" />;

}
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { useEffect } from "react";
import RoomId from "../roomId";
import { setCurrentRoom, putBoardOnStairs } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import { ReactComponent as StairsFaceUpSvg } from "./svgs/StairsFaceUp.svg";
import { RootState } from "../../app/store";
import { getFamilySelected, removeItemFromInventory } from "../../items/itemsSlice";
import ItemId from "../../items/itemId";

export default function StairsFaceUp() {
  const dispatch = useAppDispatch();
  const isBoardOnStairs = useAppSelector((state) => state.rooms.isBoardOnStairs);
  const isBoardSelected = useAppSelector((state: RootState) => getFamilySelected(state) === "Board");

  const onToPoolFaceLeftClick = () => {
    dispatch(setCurrentRoom(RoomId.PoolFaceLeft));
  };

  const onToFirstFloorFaceDown = () => {
    if (isBoardOnStairs) {
      dispatch(setCurrentRoom(RoomId.FirstFloorFaceDown));
    } else {
      dispatch(setCurrentSentences(["Nope, le trou est beaucoup trop profond."]));
    }
  };

  const onToKitchenFaceLeftClick = () => {
    dispatch(setCurrentRoom(RoomId.KitchenFaceLeft));
  };

  const onDrawingClick = () => {
    dispatch(setCurrentSentences(["Si on tourne un peu la tête, on dirait un portrait... Hmm... Non, je n'ai pas la moindre idée de ce que c'est."]));
  };

  const onHoleClick = () => {
    if (isBoardSelected) {
      dispatch(putBoardOnStairs());
      dispatch(removeItemFromInventory(["Board", 1]));
    }
  };

  const onHorrorPaintingClick = () => {
    dispatch(setCurrentSentences([
      "Ils sortent du mur quand même, les trucs, là..!"
    ]));
  };


  useEffect(() => {
    document.getElementById("toPoolFaceLeft")!.onclick = onToPoolFaceLeftClick;
    document.getElementById("Drawing")!.onclick = onDrawingClick;
    document.getElementById("toFirstFloorFaceDown")!.onclick =
      onToFirstFloorFaceDown;
    document.getElementById("toKitchenFaceLeft")!.onclick =
      onToKitchenFaceLeftClick;
    document.getElementById("Hole")!.onclick =
      onHoleClick;
    document.getElementById("HorrorPainting")!.onclick = onHorrorPaintingClick;

    if (isBoardOnStairs) {
      document.getElementById("Board")!.style.display = "block";
    } else { document.getElementById("Board")!.style.display = "none"; }

    document.getElementById("Drawing")!.onclick = onDrawingClick;
    dispatch(
      setCurrentSentences([
        "Wow, ce trou est impossible à franchir !",
        "Il semble aller direct en Enfer.",
      ])
    );
  });

  return <StairsFaceUpSvg className="RoomSVG" />;
}

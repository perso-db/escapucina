import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { useEffect } from "react";
import { cutTable, setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import RoomId from "../roomId";
import { ReactComponent as DiningRoomFaceUpSVG } from "./svgs/DiningRoomFaceUp.svg";
import { RootState } from "../../app/store";
import { getFamilySelected, removeItemFromInventory, addItemToInventory } from "../../items/itemsSlice";
import ItemId from "../../items/itemId";

export default function DiningRoomFaceUp() {
  const dispatch = useAppDispatch();
  const isTableCut = useAppSelector((state: RootState) => state.rooms.isTableCut);
  const isSawSelected = useAppSelector((state: RootState) => getFamilySelected(state) === "Saw");

  function onToKitchenFaceDownClick() {
    dispatch(setCurrentRoom(RoomId.KitchenFaceDown));
  }

  function onToLivingRoomFaceRightClick() {
    dispatch(setCurrentRoom(RoomId.LivingRoomFaceRight));
  }

  function onTableClick() {
    if (isTableCut) {
      dispatch(setCurrentSentences(["Plus rien à faire !"]));
    } else if (isSawSelected) {
      dispatch(setCurrentSentences(["Cutting table !"]));
      dispatch(cutTable());
      dispatch(removeItemFromInventory(["Saw", 1]));
      dispatch(addItemToInventory(ItemId.board1));
    } else {
      dispatch(setCurrentSentences([
        "Le bois de cette table pourrait servir de passerelle... ",
        "Mais elle est bien trop grande pour tenir dans mon sac !"
      ]));
    }
  }

  function onDoorToOutsideClick() {
    dispatch(setCurrentSentences([
      "Une porte vers l'extérieur.",
      "Dommage que les devs aient eu la flemme de programmer un extérieur."
    ]));
  }

  function onDrawingClick() {
    dispatch(setCurrentSentences([
      "Un tableau.",
      "Je n'arrive pas à me faire une opinion."
    ]));
  }

  useEffect(() => {
    document.getElementById("toKitchenFaceDown")!.onclick =
      onToKitchenFaceDownClick;
    document.getElementById("toLivingRoomFaceRight")!.onclick =
      onToLivingRoomFaceRightClick;
    document.getElementById("Table")!.onclick =
      onTableClick;
    document.getElementById("Door")!.onclick =
      onDoorToOutsideClick;
    document.getElementById("Drawing")!.onclick = onDrawingClick;
    document.getElementById("TableCut")!.style.display = (isTableCut ? "block" : "none");
  });


  useEffect(() => {
    dispatch(setCurrentSentences(["Oh ! La jolie Table"]));
  }, [dispatch]);

  return <DiningRoomFaceUpSVG className="RoomSVG" />;
}

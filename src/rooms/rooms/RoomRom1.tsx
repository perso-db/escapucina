import { useAppDispatch } from "../../app/hooks";
import { useEffect } from "react";
import RoomId from "../roomId";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import { ReactComponent as RoomRom1Svg } from "./svgs/RoomRom1.svg";

export default function RoomRom1() {
  const dispatch = useAppDispatch();

  const onToFirstFloorFaceDownClick = () => {
    dispatch(setCurrentRoom(RoomId.FirstFloorFaceDown));
  };

  const onLibraryRom1Click = () => {
    dispatch(setCurrentSentences(["Une autre page du journal. Pas utile pour me faire sortir d'ici mais intéressant."]));
  };
  const onBookPileClick = () => {
    dispatch(setCurrentSentences(["un tas haut et un tabac. Quelle bande de comique..."]));
  };

  useEffect(() => {
    document.getElementById("toFirstFloorFaceDown")!.onclick =
      onToFirstFloorFaceDownClick;
    document.getElementById("LibraryRom1")!.onclick =
      onLibraryRom1Click;
    document.getElementById("BookPile")!.onclick =
      onBookPileClick;
    dispatch(
      setCurrentSentences([
        "Une chambre de mega bo goss",
      ])
    );
  });

  return <RoomRom1Svg className="RoomSVG" />;
}

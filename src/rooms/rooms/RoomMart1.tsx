import { useAppDispatch } from "../../app/hooks";
import { useEffect } from "react";
import RoomId from "../roomId";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import { ReactComponent as RoomMart1Svg } from "./svgs/RoomMart1.svg";

export default function RoomMart1() {
  const dispatch = useAppDispatch();

  const onToFirstFloorFaceDownClick = () => {
    dispatch(setCurrentRoom(RoomId.FirstFloorFaceDown));
  };

  const onLibraryMart1Click = () => {
    dispatch(setCurrentSentences(["Une autre page du journal. Pas utile pour me faire sortir d'ici mais intéressant."]));
  };
  const onWindowClick = () => {
    dispatch(setCurrentSentences(["Hey, on voit la voiture !"]));
  };

  useEffect(() => {
    document.getElementById("toFirstFloorFaceDown")!.onclick =
      onToFirstFloorFaceDownClick;
    document.getElementById("LibraryMart1")!.onclick =
      onLibraryMart1Click;
    document.getElementById("Window")!.onclick =
      onWindowClick;
    dispatch(
      setCurrentSentences([
        "Une chambre de bo goss",
      ])
    );
  });

  return <RoomMart1Svg className="RoomSVG" />;
}

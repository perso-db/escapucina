import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { RootState } from "../../app/store";
import RoomId from "../roomId";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import { ReactComponent as PoolFaceDownSvg } from "./svgs/PoolFaceDown.svg";
import { addItemToInventory, isItemFound } from "../../items/itemsSlice";
import ItemId from "../../items/itemId";

export default function PoolFaceDown() {
  const dispatch = useAppDispatch();
  
  const isElectricalFuse2Found = useAppSelector((state: RootState) =>
  isItemFound(state, ItemId.electricalFuse2)
);

  const areTheLightsOn = useAppSelector(
    (state: RootState) => state.rooms.isLightOn
  );

  const onToHallFaceUpClick = () => {
    dispatch(setCurrentRoom(RoomId.HallFaceUp));
  };

  const onToLivingRoomFaceUpClick = () => {
    if (areTheLightsOn) {
      dispatch(setCurrentRoom(RoomId.LivingRoomFaceUp));
    } else {
      dispatch(
        setCurrentSentences([
          "Je n'irai pas plus loin sans lumière,"
        ])
      );
    }
  };

  const onToStairsFaceUpClick = () => {
    if (areTheLightsOn) {
      dispatch(setCurrentRoom(RoomId.StairsFaceUp));
    } else {
      dispatch(
        setCurrentSentences([
          "Je n'irai pas plus loin sans lumière,",
          "c'est un coup à marcher sur une mine ou un vieux clou",
          "ou un LEGO.",
        ])
      );
    }
  };

  const onToPoolFaceLeftClick = () => {
    dispatch(setCurrentRoom(RoomId.PoolFaceLeft));
  };

  const onBoat1Click = () => {
    if (!isElectricalFuse2Found) {
      dispatch(addItemToInventory(ItemId.electricalFuse2));
      dispatch(
        setCurrentSentences([
          "Une maquette de bateau, un peu fragile.",
          "Tiens, il y a quelque-chose cache la dessous...",
        ])
      );
    } else {
      dispatch(
        setCurrentSentences([
          "Oups, le mat est cassé... Cela reste beau quand meme. J'espere...",
        ])
      );
    }
  };

  const onBoat2Click = () => {
    dispatch(setCurrentSentences(["Un fameux trois mats. Il est inscrit 'Uyuni' "]));
  };

  const onBoat3Click = () => {
    dispatch(setCurrentSentences(["TrenteAnsPlusTard ? ok"]));
  };


  const onUselessMirrorClick = () => {
    dispatch(setCurrentSentences([
      "Ca a du etre un mirroir à une époque.",
      "On n'y voit rien du tout."
    ]));
  };

  const onBoxClick = () => {
    dispatch(setCurrentSentences([
      "Une boite pleine de trucs inutiles",
      "Il y a quelque-chose dedans"
      /* c'est là qu'il y a une clé ou autre objet*/
    ]));
  };

  const onJukeboxClick = () => {
    dispatch(setCurrentSentences([
      "Dire Strait et ?",
      "C'est un mec célèbre, ça, Blondin ?"
    ]));
  };

  const onPoolTableClick = () => {
    dispatch(setCurrentSentences([
      "Un vieux billard qui a servi de table d'operation ? Ironique..."
    ]));
  };
  
  useEffect(() => {
    document.getElementById("Boat1")!.onclick = onBoat1Click;
    document.getElementById("Boat2")!.onclick = onBoat2Click;
    document.getElementById("Boat3")!.onclick = onBoat3Click;
    document.getElementById("UselessMirror")!.onclick = onUselessMirrorClick;
    document.getElementById("Box")!.onclick = onBoxClick;
    document.getElementById("Jukebox")!.onclick = onJukeboxClick;
    document.getElementById("PoolTable")!.onclick = onPoolTableClick;
    
    
    document.getElementById("toHallFaceUp")!.onclick = onToHallFaceUpClick;
    document.getElementById("toStairsFaceUp")!.onclick = onToStairsFaceUpClick;
    document.getElementById("toPoolFaceLeft")!.onclick = onToPoolFaceLeftClick;
    document.getElementById("toLivingRoomFaceUp")!.onclick =
      onToLivingRoomFaceUpClick;
    dispatch(setCurrentSentences([
      "Toujours cette salle d'opération un peu glauque,",
      " on voit mal de ce côté.",
    ]));
  });

  return <PoolFaceDownSvg className="RoomSVG" />;
}

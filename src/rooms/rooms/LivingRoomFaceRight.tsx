import { useAppDispatch } from "../../app/hooks";
import { useEffect } from "react";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import RoomId from "../roomId";
import { ReactComponent as LivingRoomFaceRightSVG } from "./svgs/LivingRoomFaceRight.svg";

export default function LivingRoomFaceRight() {
  const dispatch = useAppDispatch();

  function onToPoolFaceDownClick() {
    dispatch(setCurrentRoom(RoomId.PoolFaceDown));
  }

  function onToLivingRoomFaceUpClick() {
    dispatch(setCurrentRoom(RoomId.LivingRoomFaceUp));
  }

  function onToDiningRoomFaceUpClick() {
    dispatch(setCurrentRoom(RoomId.DiningRoomFaceUp));
  }

  const onLibraryClick = () => {
    dispatch(setCurrentSentences(["Une vieille bibliothèque."]));
  };
  const onChimneyClick = () => {
    dispatch(setCurrentSentences([
      "Un message gravé dans la pierre ... 'Seul un scout peut allumer cette cheminée'. Ok...",
      "Quelques restes de prospectus à moitié brulés : mangez cinq fruits et légumes par jour... Ok."
    ]))
  };
  const onSeatsClick = () => {
    dispatch(setCurrentSentences(["On est bien là-dessus, mais ca ne va pas me faire sortir d'ici..."]));
  };
  const onSofaClick = () => {
    dispatch(setCurrentSentences(["On est bien là-dessus, mais ca ne va pas me faire sortir d'ici..."]));
  };
  const onWindow1Click = () => {
    dispatch(setCurrentSentences(["Des grilles sont à la fenêtres. Surement pour empêcher d'entrer. Ou de sortir..."]));
  };
  const onDoorClick = () => {
    dispatch(setCurrentSentences(["Une porte vérouillée. Il va falloir trouver une autre issue."]));
  };
  const onBottlesClick = () => {
    dispatch(setCurrentSentences([
      "il y a du très bon alcool là-dedans...",
      "ah, il y a du très mauvais aussi !",
      "...du Patchaquoi ?"
    ]));
  };

  useEffect(() => {
    document.getElementById("toPoolFaceDown")!.onclick = onToPoolFaceDownClick;
    document.getElementById("toLivingRoomFaceUp")!.onclick =
      onToLivingRoomFaceUpClick;
    document.getElementById("toDiningRoomFaceUp")!.onclick =
      onToDiningRoomFaceUpClick;
    document.getElementById("Library")!.onclick = onLibraryClick;
    document.getElementById("Chimney")!.onclick = onChimneyClick;
    document.getElementById("Seats")!.onclick = onSeatsClick;
    document.getElementById("Sofa")!.onclick = onSofaClick;
    document.getElementById("Window1")!.onclick = onWindow1Click;
    document.getElementById("Door")!.onclick = onDoorClick;
    document.getElementById("Bottles")!.onclick = onBottlesClick;
    dispatch(setCurrentSentences(["Ce grand salon est vraiment vide"]));

  });

  return <LivingRoomFaceRightSVG className="RoomSVG" />;
}

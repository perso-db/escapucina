import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { useEffect } from "react";
import { setCurrentRoom } from "../roomsSlice";
import { addItemToInventory, isItemFound } from "../../items/itemsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import RoomId from "../roomId";
import { ReactComponent as LivingRoomFaceUpSVG } from "./svgs/LivingRoomFaceUp.svg";
import ItemId from "../../items/itemId";
import { RootState } from "../../app/store";

export default function LivingRoomFaceUp() {
  const dispatch = useAppDispatch();
  const isKey1Found = useAppSelector((state: RootState) => isItemFound(state, ItemId.key1));

  function onToPoolFaceDownClick() {
    dispatch(setCurrentRoom(RoomId.PoolFaceDown));
  }
  function onToDiningRoomFaceUpClick() {
    dispatch(setCurrentRoom(RoomId.DiningRoomFaceUp));
  }
  function onToLivingRoomFaceRightClick() {
    dispatch(setCurrentRoom(RoomId.LivingRoomFaceRight));
  }

  const onSeats2Click = () => {
    if (!isKey1Found) {
      dispatch(setCurrentSentences(["On est bien la-dessus, mais ça ne va pas me faire sortir d'ici...   Oh ! Une clef !"]));
      dispatch(addItemToInventory(ItemId.key1));
    } else {
      dispatch(setCurrentSentences(["On est bien la-dessus, mais ça ne va pas me faire sortir d'ici..."]));
    }
  };
  const onSeats1Click = () => {
    dispatch(setCurrentSentences(["On est bien là-dessus, mais ça ne va pas me faire sortir d'ici..."]));
  };
  const onSofa1Click = () => {
    dispatch(setCurrentSentences(["On est bien là-dessus, mais ça ne va pas me faire sortir d'ici..."]));
  };
  const onSofa2Click = () => {
    dispatch(setCurrentSentences(["On est bien là-dessus, mais ça ne va pas me faire sortir d'ici..."]));
  };

  const onChimneyClick = () => {
    dispatch(setCurrentSentences([
      "un message gravé dans la pierre ... 'Seul un scout peut allumer cette cheminée'. Ok...",
      "Quelques restes de prospectus à moitié brulés : mangez cinq fruits et légumes par jour... Ok."
    ]))
  };
  const onWindow1Click = () => {
    dispatch(setCurrentSentences(["Des grilles sont à la fenêtres. Surement pour empêcher d'entrer. Ou de sortir..."]));
  };
  const onWindow2Click = () => {
    dispatch(setCurrentSentences(["Des grilles sont à la fenêtres. Surement pour empêcher d'entrer. Ou de sortir..."]));
  };
  const onWindow3Click = () => {
    dispatch(setCurrentSentences(["Des grilles sont à la fenêtres. Surement pour empêcher d'entrer. Ou de sortir..."]));
  };
  const onPianoClick = () => {
    dispatch(setCurrentSentences(["Il n'est pas accordé. Ceci dit, c'est la première fois que je joue du piano."]));
  };
  const onBottlesClick = () => {
    dispatch(setCurrentSentences([
      "il y a du très bon alcool là-dedans...",
      "ah, il y a du très mauvais aussi !",
      "...Du Patchaquoi ?"
    ]));
  };
  const onPhoneLampClick = () => {
    dispatch(setCurrentSentences([
      "C'est un téléhone-lampe ? Ou une lampe-téléphone ?"
    ]));
  };




  /* ajouter Lampe telephone*/

  useEffect(() => {
    document.getElementById("toPoolFaceDown")!.onclick = onToPoolFaceDownClick;
    document.getElementById("toDiningRoomFaceUp")!.onclick = onToDiningRoomFaceUpClick;
    document.getElementById("toLivingRoomFaceRight")!.onclick = onToLivingRoomFaceRightClick;
    document.getElementById("Seats2")!.onclick = onSeats2Click;
    document.getElementById("Seats1")!.onclick = onSeats1Click;
    document.getElementById("Sofa1")!.onclick = onSofa1Click;
    document.getElementById("Chimney")!.onclick = onChimneyClick;
    document.getElementById("Window1")!.onclick = onWindow1Click;
    document.getElementById("Window2")!.onclick = onWindow2Click;
    document.getElementById("Window3")!.onclick = onWindow3Click;
    document.getElementById("Sofa2")!.onclick = onSofa2Click;
    document.getElementById("Piano")!.onclick = onPianoClick;
    document.getElementById("Bottles")!.onclick = onBottlesClick;
    document.getElementById("PhoneLamp")!.onclick = onPhoneLampClick;
  });

  useEffect(() => {
    dispatch(
      setCurrentSentences([
        "Le salon",
        "avec des beaux canapes",
        "et une cheminee",
        "poil au nez",
      ])
    );
  }, [dispatch]);

  return <LivingRoomFaceUpSVG className="RoomSVG" />;
}

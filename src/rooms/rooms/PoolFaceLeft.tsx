import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { useEffect } from "react";
import RoomId from "../roomId";
import { setCurrentRoom } from "../roomsSlice";
import ItemId from "../../items/itemId";
import { addItemToInventory, isItemFound } from "../../items/itemsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import { ReactComponent as PoolFaceLeftSvg } from "./svgs/PoolFaceLeft.svg";
import { RootState } from "../../app/store";

export default function PoolFaceLeft() {
  const dispatch = useAppDispatch();
  const isElectricalFuse2Found = useAppSelector((state: RootState) =>
    isItemFound(state, ItemId.electricalFuse2)
  );
  const areTheLightsOn = useAppSelector(
    (state: RootState) => state.rooms.isLightOn
  );

  const onToHallFaceUpClick = () => {
    dispatch(setCurrentRoom(RoomId.HallFaceUp));
  };

  const onToLivingRoomFaceUpClick = () => {
    if (areTheLightsOn) {
      dispatch(setCurrentRoom(RoomId.LivingRoomFaceUp));
    } else {
      dispatch(
        setCurrentSentences([
          "Je n'irai pas plus loin sans lumière,",
          "c'est un coup a marcher sur une mine ou un vieux clou",
          "ou un LEGO",
        ])
      );
    }
  };

  const onToPoolFaceDownClick = () => {
    dispatch(setCurrentRoom(RoomId.PoolFaceDown));
  };

  const onToStairsFaceUpClick = () => {
    if (areTheLightsOn) {
      dispatch(setCurrentRoom(RoomId.StairsFaceUp));
    } else {
      dispatch(
        setCurrentSentences([
          "Je n'irai pas plus loin sans lumière,",
          "c'est un coup a marcher sur une mine ou un vieux clou",
          "ou un LEGO",
        ])
      );
    }
  };

  const onBookShelf1Click = () => {
    dispatch(setCurrentSentences(["De vieux livres de médecine tout écornés."]));
  };

  const onBookShelf2Click = () => {
    dispatch(setCurrentSentences([
      "De vieux livres de psychologie,",
      "feuilletés par endroits."]));
  };

  const onBookShelf3Click = () => {
    dispatch(addItemToInventory(ItemId.electricalFuse3));
    dispatch(setCurrentSentences([
      "La communication pour les nuls",
      "jamais ouvert, on dirait",]));
  };

  const onBookShelf4Click = () => {
    dispatch(addItemToInventory(ItemId.electricalFuse4));
    dispatch(setCurrentSentences([
      "La cuisine vegan pour les nuls",
      "encore dans son emballage.",
      "Quelqu'un a dessiné une petite tête de mort dessus. Sympa."
    ]));
  };

  const onBookShelf5Click = () => {
    dispatch(setCurrentSentences([
      '"Une vague pour Manu" ? ',
      'Jamais entendu parler mais il y est 14 fois... ',
      'ca doit etre drolement bien, ou nul.'
    ]));
  };

  const onBoat1Click = () => {
    if (!isElectricalFuse2Found) {
      dispatch(addItemToInventory(ItemId.electricalFuse2));
      dispatch(
        setCurrentSentences([
          "Une maquette de bateau, un peu fragile.",
          "Tiens, il y a quelque-chose cache la dessous...",
        ])
      );
    } else {
      dispatch(
        setCurrentSentences([
          "Oups, le mât est cassé... Cela reste beau quand meme. J'espère...",
        ])
      );
    }
  };

  const onBoat2Click = () => {
    dispatch(setCurrentSentences(["C'est un fameux trois mâts. Il y a écrit 'Uyuni' ."]));
  };

  const onPoolTableClick = () => {
    dispatch(setCurrentSentences(["Un vieux billard qui a servi de table d'operation ? Ironique..."]));
  };

  const onFrame1Click = () => {
    dispatch(setCurrentSentences(["Un cadre. On n'est pas obligés de faire des blagues à chaque fois, si ?"]));
  };

  const onFrame2Click = () => {
    dispatch(setCurrentSentences(["Non mais le goût de gens qui vivaient ici..."]));
  };

  useEffect(() => {
    document.getElementById("toHallFaceUp")!.onclick = onToHallFaceUpClick;
    document.getElementById("toPoolFaceDown")!.onclick = onToPoolFaceDownClick;
    document.getElementById("toStairsFaceUp")!.onclick = onToStairsFaceUpClick;
    document.getElementById("toLivingRoomFaceUp")!.onclick = onToLivingRoomFaceUpClick;
    document.getElementById("BookShelf1")!.onclick = onBookShelf1Click;
    document.getElementById("BookShelf2")!.onclick = onBookShelf2Click;
    document.getElementById("BookShelf3")!.onclick = onBookShelf3Click;
    document.getElementById("BookShelf4")!.onclick = onBookShelf4Click;
    document.getElementById("BookShelf5")!.onclick = onBookShelf5Click;
    document.getElementById("Boat1")!.onclick = onBoat1Click;
    document.getElementById("Boat2")!.onclick = onBoat2Click;
    document.getElementById("PoolTable")!.onclick = onPoolTableClick;
    document.getElementById("Frame1")!.onclick = onFrame1Click;
    document.getElementById("Frame2")!.onclick = onFrame2Click;
  });

  useEffect(() => {
    dispatch(setCurrentSentences([
      "On dirait une vieille salle d'operation,",
      "Il doit bien y avoir quelques trucs utiles a recuperer.."]));
  }, [dispatch]);

  return <PoolFaceLeftSvg className="RoomSVG" />;
}

import { useAppDispatch } from "../../app/hooks";
import { useEffect } from "react";
import { setCurrentRoom } from "../roomsSlice";
import RoomId from "../roomId";
import { setCurrentSentences } from "../../narration/narrationSlice";
import { ReactComponent as FirstFloorFaceDownSvg } from "./svgs/FirstFloorFaceDown.svg";

export default function StairsFaceUp() {
  const dispatch = useAppDispatch();

  const onToStairsFaceUpClick = () => {
    dispatch(setCurrentRoom(RoomId.StairsFaceUp));
  };

  const onToRoomRom1Click = () => {
    dispatch(setCurrentRoom(RoomId.RoomRom1));
  };
  const onToRoomMart1Click = () => {
    dispatch(setCurrentRoom(RoomId.RoomMart1));
  };
  const onToRoomTiph1Click = () => {
    dispatch(setCurrentRoom(RoomId.RoomTiph1));
  };
  const onToRoomGui1Click = () => {
    dispatch(setCurrentRoom(RoomId.RoomGui1));
  };

  const onShoesClick = () => {
    dispatch(setCurrentSentences([
      "Un drôle de meuble à chaussures."]));
  };

  const onClosetClick = () => {
    dispatch(setCurrentSentences([
      "Un placard. Tiens, encore une de ces bandelettes."]));
  };

  const onBoxClick = () => {
    dispatch(setCurrentSentences([
      "Un jeu d'échec et une note : 'Une seule pièce vous manque et tout est dépeuplé.'"]));
  };

  useEffect(() => {
    document.getElementById("toStairsFaceUp")!.onclick = onToStairsFaceUpClick;
    document.getElementById("toRoomRom1")!.onclick = onToRoomRom1Click;
    document.getElementById("toRoomMart1")!.onclick = onToRoomMart1Click;
    document.getElementById("toRoomGui1")!.onclick = onToRoomGui1Click;
    document.getElementById("toRoomTiph1")!.onclick = onToRoomTiph1Click;
    document.getElementById("Shoes")!.onclick = onShoesClick;
    document.getElementById("Closet")!.onclick = onClosetClick;
    document.getElementById("Box")!.onclick = onBoxClick;

    dispatch(
      setCurrentSentences([
        "On dirait un grand espace de jeu",
        "mal range."
      ])
    );
  });

  return <FirstFloorFaceDownSvg className="RoomSVG" />;
}

import { useEffect } from "react";
import { RootState } from "../../app/store";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import RoomId from "../roomId";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import ItemId from "../../items/itemId";
import {
  addItemToInventory,
  isItemFound,
} from "../../items/itemsSlice";
import { isFinished } from "../miniGames/electricalPanelSlice";
import { ReactComponent as HallFaceUpSVG } from "./svgs/HallFaceUp.svg";

export default function HallFaceUp() {
  const dispatch = useAppDispatch();
  const isElectricalFuse1Found = useAppSelector((state: RootState) =>
    isItemFound(state, ItemId.electricalFuse1)
  );

  const onToPoolFaceLeftClick = () => {
    dispatch(setCurrentRoom(RoomId.PoolFaceLeft));
  };

  const onToHallFaceDownClick = () => {
    dispatch(setCurrentRoom(RoomId.HallFaceDown));
  };

  const onTreeOfHatClick = () => {
    dispatch(
      setCurrentSentences([
        "On dirait que ces chapeaux sont là depuis une éternité. Intrigant…",
        "J'en prendrais bien un mais on n'est pas le 2 mai, ça porte malheur.",
        "Ah tiens, une bandelette de papier ?", /*!!! que dans le premier texte !!!*/
      ])
    );
  };

  const onHallFrameClick = () => {
    if (!isElectricalFuse1Found) {
      dispatch(addItemToInventory(ItemId.electricalFuse1));
      dispatch(
        setCurrentSentences([
          "Un collier de nouilles, mais en dessin.",
          "Tiens, il y a quelque-chose de cache dessous...",
        ])
      );
    } else {
      dispatch(
        setCurrentSentences([
          "Toujours le collier de nouilles, toujours en dessin.",
        ])
      );
    }
  };

  const isElectricalPanelFinished = useAppSelector((state: RootState) =>
    isFinished(state)
  );
  const onToDisjoncteurClick = () => {
    if (!isElectricalPanelFinished) {
      dispatch(setCurrentRoom(RoomId.ElectricalPanel));
    } else {
      dispatch(
        setCurrentSentences(["La lumière est allumee, plus besoin de ca..."])
      );
    }
  };

  useEffect(() => {
    document.getElementById("toPoolFaceLeft")!.onclick = onToPoolFaceLeftClick;
    document.getElementById("toHallFaceDown")!.onclick = onToHallFaceDownClick;
    document.getElementById("TreeOfHat")!.onclick = onTreeOfHatClick;
    document.getElementById("HallFrame")!.onclick = onHallFrameClick;
    document.getElementById("toDisjoncteur")!.onclick = onToDisjoncteurClick;
  });

  useEffect(() => {
    dispatch(setCurrentSentences([
      "Difficile d'avancer, on n'y voit rien du tout. ",
      "Cet endroit ne me dit rien qui vaille, je ne sais pas ce qui traine...",
      "Il vaut mieux ne pas trop m'éloigner avant d'avoir remis un peu de lumière.",
      "Cherchons.",
    ]));
  }, [dispatch]);

  return <HallFaceUpSVG className="RoomSVG" />;
}

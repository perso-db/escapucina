import { useAppDispatch } from "../../app/hooks";
import { ReactComponent as RoomTiph1Svg } from "./svgs/RoomTiph1.svg";
import { useEffect } from "react";
import { setCurrentRoom } from "../roomsSlice";

import RoomId from "../roomId";
import { setCurrentSentences } from "../../narration/narrationSlice";

export default function RoomTiph1() {
  const dispatch = useAppDispatch();

  const onToFirstFloorFaceDownClick = () => {
    dispatch(setCurrentRoom(RoomId.FirstFloorFaceDown));
  };

  const onLibraryTiph1Click = () => {
    dispatch(setCurrentSentences(["Une autre page du journal. Pas utile pour me faire sortir d'ici mais intéressant."]));
  };
  const onPieceOnFloorClick = () => {
    dispatch(setCurrentSentences(["Un cavalier du jeu d'échec. Que fait-il ici ?"]));
  };

  useEffect(() => {
    document.getElementById("toFirstFloorFaceDown")!.onclick =
      onToFirstFloorFaceDownClick;
    document.getElementById("LibraryTiph1")!.onclick = onLibraryTiph1Click;
    document.getElementById("PieceOnFloor")!.onclick = onPieceOnFloorClick;
    dispatch(
      setCurrentSentences(["Une chambre de gonzesse, mais sans le rose"])
    );
  });

  return <RoomTiph1Svg className="RoomSVG" />;
}

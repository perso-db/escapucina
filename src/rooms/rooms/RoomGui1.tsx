import { useAppDispatch } from "../../app/hooks";
import { ReactComponent as RoomGui1Svg } from "./svgs/RoomGui1.svg";
import { useEffect } from "react";
import { setCurrentRoom } from "../roomsSlice";

import RoomId from "../roomId";
import { setCurrentSentences } from "../../narration/narrationSlice";

export default function RoomGui1() {
  const dispatch = useAppDispatch();

  const onToFirstFloorFaceDownClick = () => {
    dispatch(setCurrentRoom(RoomId.FirstFloorFaceDown));
  };

  const onLibraryGuiClick = () => {
    dispatch(setCurrentSentences(["Capitaine Tsubasa, Tom-Tom et Nana, et d'autres bouquins pour enfant"]));
  };

  const onDrawersGuiClick = () => {
    dispatch(setCurrentSentences(["Des affaires de foot et des t-shirts beaucoup trop grand pour un enfant"]));
  };

  useEffect(() => {
    document.getElementById("toFirstFloorFaceDown")!.onclick =
      onToFirstFloorFaceDownClick;
    document.getElementById("LibraryGui")!.onclick = onLibraryGuiClick;
    document.getElementById("DrawersGui")!.onclick = onDrawersGuiClick;
    dispatch(setCurrentSentences(["La chambre de Gui1"]));
  });

  return <RoomGui1Svg className="RoomSVG" />;
}

import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { useEffect, useState } from "react";
import { setCurrentRoom } from "../roomsSlice";
import { setCurrentSentences } from "../../narration/narrationSlice";
import RoomId from "../roomId";
import { ReactComponent as DrawersSVG } from "./svgs/Drawers.svg";
import { isItemFound, addItemToInventory } from "../../items/itemsSlice";
import { RootState } from "../../app/store";
import ItemId from "../../items/itemId";

export default function Drawers() {
  const dispatch = useAppDispatch();
  const [uselessItemQuantity, setUselessItemQuantity] = useState(0);
  const isSawFound = useAppSelector((state: RootState) => isItemFound(state, ItemId.saw1));

  function onToKitchenFaceDownClick() {
    dispatch(setCurrentRoom(RoomId.KitchenFaceDown));
  }

  const onDrawersSearchClick = () => {
    if (uselessItemQuantity < uselessItemText.length) {
      let uselessText = uselessItemText[uselessItemQuantity];
      console.debug(uselessText);
      dispatch(setCurrentSentences([uselessText]));
      setUselessItemQuantity(uselessItemQuantity + 1);
    } else if (!isSawFound) {
      console.debug("Finding scie");
      dispatch(setCurrentSentences(["Une mauvaise blague ... Ah, une scie !"]));
      dispatch(addItemToInventory(ItemId.saw1));
    } else {
      dispatch(setCurrentSentences(["Il n'y a plus rien d'utile"]));
    }
  };

  const uselessItemText = [
    "On a tous un tiroir comme ça quelque-part",
    "Et c'est pas fini ",
    "Il reste deux trois bricoles au fond !",
    "C'est sans fin",
    "On dirait une blague",
  ];


  useEffect(() => {
    document.getElementById("toKitchenFaceDown")!.onclick = onToKitchenFaceDownClick;
    document.getElementById("DrawersSearch1")!.onclick = onDrawersSearchClick;
    document.getElementById("DrawersSearch2")!.onclick = onDrawersSearchClick;
    document.getElementById("DrawersSearch3")!.onclick = onDrawersSearchClick;
  });

  useEffect(() => {
    dispatch(setCurrentSentences([
      "Des tiroirs clairement en bordel...",
    ]));
  }, [dispatch]);

  return <DrawersSVG className="RoomSVG" />;
}

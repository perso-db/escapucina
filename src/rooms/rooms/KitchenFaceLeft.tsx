import { useAppDispatch } from "../../app/hooks";
import { ReactComponent as KitchenFaceLeftSvg } from "./svgs/KitchenFaceLeft.svg";
import { useEffect } from "react";
import { setCurrentRoom } from "../roomsSlice";
import RoomId from "../roomId";
import { setCurrentSentences } from "../../narration/narrationSlice";

export default function KitchenFaceLeft() {
  const dispatch = useAppDispatch();

  const onToDiningRoomFaceUpClick = () => {
    dispatch(setCurrentRoom(RoomId.DiningRoomFaceUp));
  };

  const onToStairsFaceUpClick = () => {
    dispatch(setCurrentRoom(RoomId.StairsFaceUp));
  };

  const onToKitchenFaceDownClick = () => {
    dispatch(setCurrentRoom(RoomId.KitchenFaceDown));
  };

  const onFrigogidaireClick = () => {
    dispatch(setCurrentRoom(RoomId.Frigogidaire));
  };

  const onDrawer1Click = () => {
    dispatch(setCurrentSentences([
      "Ah une peluche !",
      "il n'y a plus rien dans ce tirroir" /* quand on l'a trouvé */
    ]));
  };
  const onDrawer2Click = () => {
    dispatch(setCurrentSentences([
      "Des conserves.",
      "De quoi faire deux canards entiers, en pièces détachées"
    ]));
  };

  const onScaleClick = () => {
    dispatch(setCurrentSentences([
      "Une balance à légumes, n'a visiblement jamais servi."
    ]));
  };

  const onPostalCardsClick = () => {
    dispatch(setCurrentSentences([
      "\"L'afrique est bonne hotesse?\""
    ]));
  };
  const onDrawingClick = () => {
    dispatch(setCurrentSentences([
      "Marrant, ça par contre c'est pas mal !",
      "p'tetre le seul truc qui vaut quelque chose dans cette maison."
    ]));
  };



  useEffect(() => {
    document.getElementById("toStairsFaceUp")!.onclick = onToStairsFaceUpClick;
    document.getElementById("toDiningRoomFaceUp")!.onclick =
      onToDiningRoomFaceUpClick;
    document.getElementById("toKitchenFaceDown")!.onclick =
      onToKitchenFaceDownClick;
    document.getElementById("frigogidaire")!.onclick = onFrigogidaireClick;
    document.getElementById("Drawer1")!.onclick = onDrawer1Click;
    document.getElementById("Drawer2")!.onclick = onDrawer2Click;
    document.getElementById("Scale")!.onclick = onScaleClick;
    document.getElementById("PostalCards")!.onclick = onPostalCardsClick;
    document.getElementById("Drawing")!.onclick = onDrawingClick;

    dispatch(setCurrentSentences(["Cette cuisine est une catastrophe,", "un couloir, c'est pas vraiment pratique..."]));
  });

  return <KitchenFaceLeftSvg className="RoomSVG" />;
}

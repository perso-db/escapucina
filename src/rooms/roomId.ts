enum RoomId {
  HallFaceUp = "HallFaceUp",
  PoolFaceLeft = "PoolFaceLeft",
  PoolFaceDown = "PoolFaceDown",
  LivingRoomFaceUp = "LivingRoomFaceUp",
  LivingRoomFaceRight = "LivingRoomFaceRight",
  StairsFaceUp = "StairsFaceUp",
  HallFaceDown = "HallFaceDown",
  KitchenFaceLeft = "KitchenFaceLeft",
  KitchenFaceDown = "KitchenFaceDown",
  Drawers = "Drawers",
  Frigogidaire = "Frigogidaire",
  DiningRoomFaceUp = "DiningRoomFaceUp",

  FirstFloorFaceDown = "FirstFloorFaceDown",
  RoomRom1 = "RoomRom1",
  RoomTiph1 = "RoomTiph1",
  RoomMart1 = "RoomMart1",
  RoomGui1 = "RoomGui1",

  ElectricalPanel = "ElectricalPanel",
  Bandelettes = "Bandelettes",
  Lock = "Lock",
}

export default RoomId;

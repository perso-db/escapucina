import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../app/store";
import RoomId from "./roomId";

interface RoomsState {
  currentRoomId: RoomId;
  roomsAvailable: Array<RoomId>;
  isLightOn: boolean;
  isFinalDoorHandleOn: boolean;
  isBoardOnStairs: boolean,
  areDrawersOpen: boolean,
  isTableCut: boolean,
}

const initialState: RoomsState = {
  currentRoomId: RoomId.HallFaceUp,
  roomsAvailable: [
    RoomId.HallFaceUp,
    RoomId.HallFaceDown,
    RoomId.PoolFaceLeft,
    RoomId.PoolFaceDown,
    RoomId.LivingRoomFaceUp,
    RoomId.StairsFaceUp,
  ],
  isLightOn: true,
  isFinalDoorHandleOn: false,
  isBoardOnStairs: false,
  areDrawersOpen: false,
  isTableCut: false,
};

const RoomsSlice = createSlice({
  name: "rooms",
  initialState,
  reducers: {
    setCurrentRoom(state, action: PayloadAction<RoomId>) {
      state.currentRoomId = action.payload;
    },
    addAvailableRoom(state, action: PayloadAction<RoomId>) {
      state.roomsAvailable.push(action.payload);
    },
    turnTheLightsOn(state) {
      state.isLightOn = true;
    },
    putFinalDoorHandle(state) {
      state.isFinalDoorHandleOn = true;
    },
    putBoardOnStairs(state) {
      state.isBoardOnStairs = true;
    },
    openDrawers(state) {
      state.areDrawersOpen = true;
    },
    cutTable(state) {
      state.isTableCut = true;
    }
  },
});

export const {
  setCurrentRoom,
  addAvailableRoom,
  turnTheLightsOn,
  putFinalDoorHandle,
  putBoardOnStairs,
  openDrawers,
  cutTable,
} = RoomsSlice.actions;

export const getCurrentRoomId = (state: RootState) => {
  return state.rooms.currentRoomId;
};

export default RoomsSlice.reducer;

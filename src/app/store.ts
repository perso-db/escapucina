import { AnyAction, combineReducers } from "redux";
import { configureStore, ThunkAction, Action, Reducer } from "@reduxjs/toolkit";
import gameReducer from "../game/gameSlice";
import roomsReducer from "../rooms/roomsSlice";
import narrationReducer from "../narration/narrationSlice";
import mapReducer from "../map/mapSlice";
import itemsReducer from "../items/itemsSlice";
import electricalPanelReducer from "../rooms/miniGames/electricalPanelSlice";
import bandelettesReducer from "../rooms/miniGames/bandelettesSlice";
import lockReducer from "../rooms/miniGames/lockSlice";
import pageReducer from "../items/pageSlice";

const combinedReducers = combineReducers({
  game: gameReducer,
  rooms: roomsReducer,
  narration: narrationReducer,
  map: mapReducer,
  items: itemsReducer,
  electricalPanel: electricalPanelReducer,
  bandelettes: bandelettesReducer,
  lock: lockReducer,
  page: pageReducer,
});

const rootReducer: Reducer = (state: RootState, action: AnyAction) => {
  if (action.type === "game/resetGame") {
    state = {} as RootState;
  }
  return combinedReducers(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof combinedReducers>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

import { createSlice } from "@reduxjs/toolkit";

interface NarrationState {
  currentSentences: Array<string>;
}

const initialState: NarrationState = {
  currentSentences: ["Le Hall"],
};

const narrationSlice = createSlice({
  name: "narration",
  initialState,
  reducers: {
    setCurrentSentences(state, action) {
      state.currentSentences = action.payload;
    },
  },
});

export const { setCurrentSentences } = narrationSlice.actions;

export default narrationSlice.reducer;

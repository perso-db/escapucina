import { useAppSelector } from "../app/hooks";
import { RootState } from "../app/store";
import "./Narration.css";

export default function Narration() {
  const sentences = useAppSelector(
    (state: RootState) => state.narration.currentSentences
  );

  function renderText () {
    let textArray: Array<JSX.Element> = [];
    for (const index in sentences) {
        textArray.push(
          <div key={"sentence" + index}>{sentences[index]}</div>
        )
  }
return textArray}

  return (
    <div className="Narration">
      <div className="textZone">
        <span className="text" id="currentText">
        {renderText()}
        </span>
      </div>
    </div>
  );
}

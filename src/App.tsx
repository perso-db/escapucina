import { useSelector } from "react-redux";

import "./App.css";
import { getGameStatus, GameStatus } from "./game/gameSlice";
import GamingBoard from "./game/GamingBoard";
import StartScreen from "./game/StartScreen";
import VictoryScreen from "./game/VictoryScreen";
import IntroductionSequence from "./game/IntroductionSequence";

function MainScreen(status: GameStatus) {
  switch (status) {
    case GameStatus.start:
      return <StartScreen />;
    case GameStatus.playing:
      return <GamingBoard />;
    case GameStatus.outro:
      return <VictoryScreen />;
    case GameStatus.intro:
      return <IntroductionSequence />
  }
}

function App() {
  const gameStatus = useSelector(getGameStatus);
  return <div className="App">{MainScreen(gameStatus)}</div>;
}

export default App;

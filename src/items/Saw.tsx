import { ReactComponent as SawSVG } from "./svgs/Saw.svg";

export default function Saw() {
    return <SawSVG className="itemImage" />;
}

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../app/store";
import ItemId from "./itemId";
import { itemsFamily } from "../items/ItemOpened";

export type itemsIventoryDict = {
  [key in string]: Array<number>;
}

interface itemsState {
  familyOpened: string | undefined;
  itemsFound: Array<ItemId>;
  itemsInventory: itemsIventoryDict;
  familySelected: string | undefined;
}

const initialState: itemsState = {
  familyOpened: undefined,
  itemsFound: [],
  itemsInventory: {},
  familySelected: undefined,
};

const index = (itemId: ItemId) => {
  return parseInt(itemId.toString().replace(/^\D+/g, ''))
}

const itemsSlice = createSlice({
  name: "items",
  initialState,
  reducers: {
    openFamily(state, action: PayloadAction<string>) {
      state.familyOpened = action.payload;
    },
    closeFamily(state) {
      state.familyOpened = undefined;
    },
    selectFamily(state, action: PayloadAction<string>) {
      state.familySelected = action.payload;
    },
    unselectFamily(state) {
      state.familySelected = undefined;
    },
    addItemToInventory(state, action: PayloadAction<ItemId>) {
      const itemId = action.payload;
      if (state.itemsFound.indexOf(itemId) === -1) {
        var family = itemsFamily[itemId];
        var indexes = state.itemsInventory[family];
        if (indexes === undefined) {
          state.itemsInventory[family] = [index(itemId)];
        } else {
          indexes.push(index(itemId));
        }
        state.itemsFound.push(itemId);
      }
    },
    removeItemFromInventory(state, action: PayloadAction<[string, number]>) {
      var indexes = state.itemsInventory[action.payload[0]];
      indexes.splice(indexes.indexOf(action.payload[1]), 1);
      if (indexes.length === 0) {
        delete state.itemsInventory[action.payload[0]];
      }
      if (state.familySelected === action.payload[0]) {
        state.familySelected = undefined;
      }
    },
  },
});

export const {
  openFamily,
  closeFamily,
  selectFamily,
  unselectFamily,
  addItemToInventory,
  removeItemFromInventory,
} = itemsSlice.actions;

export const getFamilyOpened = (state: RootState) => {
  return state.items.familyOpened;
};

export const getFamilySelected = (state: RootState) => {
  return state.items.familySelected;
};

export const getItemsInventory = (state: RootState) => {
  return state.items.itemsInventory;
};

export const isItemFound = (state: RootState, itemId: ItemId) => {
  return state.items.itemsFound.indexOf(itemId) !== -1;
};

export default itemsSlice.reducer;

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../app/store";

interface PageState {
    pagesAvailable: Array<number>;
    currentPage: number;
}

const initialState: PageState = {
    pagesAvailable: [],
    currentPage: -1,
};

const lockSlice = createSlice({
    name: "page",
    initialState,
    reducers: {
        setCurrentPage(state, action: PayloadAction<number>) {
            state.currentPage = action.payload;
        },
        findPage(state, action: PayloadAction<number>) {
            state.pagesAvailable.push(action.payload)
        }
    },
});

export const { setCurrentPage, findPage } = lockSlice.actions;

export const getCurrentPage = (state: RootState) => {
    return state.page.currentPage;
};

export const getPagesAvailable = (state: RootState) => {
    return state.page.pagesAvailable;
};

export const pageTexts = [
    "texte de la page 1",
    "texte de la page 2",
    "texte de la page 3",
    "texte de la page 4",
    "texte de la page 5",
    "texte de la page 6",
    "texte de la page 7",
    "texte de la page 8",
    "texte de la page 9",
    "texte de la page 10",
]

export default lockSlice.reducer;

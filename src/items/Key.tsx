import { ReactComponent as KeySVG } from "./svgs/Key.svg";

export default function Key() {
  return <KeySVG className="itemImage" />;
}

import { useAppSelector, useAppDispatch } from "../app/hooks";
import { RootState } from "../app/store";

import { closeFamily } from "./itemsSlice";
import ElectricalFuse from "./ElectricalFuse";
import Strip from "./Strip";
import Key from "./Key";
import Page from "./Page";
import Board from "./Board";
import Saw from "./Saw";

import ItemId from "./itemId";
import "./ItemOpened.css";
import { useEffect } from "react";
import { setCurrentSentences } from "../narration/narrationSlice";

type ItemsFamily = {
  [key in ItemId]: string;
};

export const itemsFamily: ItemsFamily = {
  [ItemId.electricalFuse1]: "ElectricalFuse",
  [ItemId.electricalFuse2]: "ElectricalFuse",
  [ItemId.electricalFuse3]: "ElectricalFuse",
  [ItemId.electricalFuse4]: "ElectricalFuse",
  [ItemId.bande1]: "Strip",
  [ItemId.bande2]: "Strip",
  [ItemId.bande3]: "Strip",
  [ItemId.bande4]: "Strip",
  [ItemId.bande5]: "Strip",
  [ItemId.bande6]: "Strip",
  [ItemId.bande7]: "Strip",
  [ItemId.page1]: "Page",
  [ItemId.page2]: "Page",
  [ItemId.page3]: "Page",
  [ItemId.page4]: "Page",
  [ItemId.page5]: "Page",
  [ItemId.page6]: "Page",
  [ItemId.page7]: "Page",
  [ItemId.page8]: "Page",
  [ItemId.page9]: "Page",
  [ItemId.page10]: "Page",
  [ItemId.key1]: "Key",
  [ItemId.board1]: "Board",
  [ItemId.saw1]: "Saw",
};

type FamilyToElement = {
  [key in string]: JSX.Element;
};

export const familyToElement: FamilyToElement = {
  "ElectricalFuse": <ElectricalFuse />,
  "Strip": <Strip />,
  "Page": <Page />,
  "Key": <Key />,
  "Board": <Board />,
  "Saw": <Saw />,
};

type ItemsText = {
  [key in string]: string;
};
export const itemsText: ItemsText = {
  "ElectricalFuse": "Un fusible, il a l'air en bon etat.",
  "Strip": "Une bande, qui ressemble a s'y meprendre a un fusible.",
  "Page": "Une page de toute beauté",
  "Key": "Une cle, de sol ou du bonheur, c'est au choix",
};

export default function ItemOpened() {
  const dispatch = useAppDispatch();
  const familyOpenedId = useAppSelector(
    (state: RootState) => state.items.familyOpened
  );
  var itemOpenedText = "";
  if (familyOpenedId) {
    itemOpenedText = itemsText[familyOpenedId];
  }

  useEffect(() => {
    if (itemOpenedText !== "") {
      dispatch(setCurrentSentences([itemOpenedText]));
    }
  });

  function onBackgroundClick() {
    dispatch(closeFamily());
  }

  if (familyOpenedId) {
    return (
      <div className="ItemBackground" onClick={onBackgroundClick}>
        <div
          className="ItemOpened"
          onClick={(event) => event.stopPropagation()}
        >
          {familyToElement[familyOpenedId]}
        </div>
      </div>
    );
  } else {
    return <div />;
  }
}

import { ReactComponent as StripSVG } from "./svgs/Strip.svg";

export default function Strip() {
  return <StripSVG className="itemImage" />;
}

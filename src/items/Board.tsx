import { ReactComponent as BoardSVG } from "./svgs/Board.svg";

export default function Board() {
  return <BoardSVG className="itemImage" />;
}

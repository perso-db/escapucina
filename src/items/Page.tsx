import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { RootState } from "../app/store";

import {
    getPagesAvailable, getCurrentPage, setCurrentPage, pageTexts
} from "./pageSlice";

import { ReactComponent as PageSVG } from "./svgs/Key.svg";

export default function Page() {
    const dispatch = useAppDispatch();
    const pagesAvailable = useAppSelector((state: RootState) => getPagesAvailable(state));
    const currentPage = useAppSelector((state: RootState) => getCurrentPage(state));


    const nextPage = () => {
        var max_index = pagesAvailable.length;
        var current_index = pagesAvailable.indexOf(currentPage);
        var new_index = (current_index + 1) % max_index;
        var new_page = pagesAvailable[new_index]
        dispatch(setCurrentPage(new_page))
    }

    const previousPage = () => {
        var max_index = pagesAvailable.length;
        var current_index = pagesAvailable.indexOf(currentPage);
        var new_index = (current_index - 1) % max_index;
        var new_page = pagesAvailable[new_index]
        dispatch(setCurrentPage(new_page))
    }

    const connectButtons = () => {
        document.getElementById("next")!.onclick = () => nextPage();
        document.getElementById("previous")!.onclick = () => previousPage();
    }

    const displayText = () => {
        document.getElementById("text")!.innerText = pageTexts[currentPage];
    }

    useEffect(() => {
        connectButtons();
        displayText();
    })

    return <PageSVG className="itemImage" />;
}

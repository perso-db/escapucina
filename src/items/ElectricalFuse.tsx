import { ReactComponent as ElectricalFuseSVG } from "./svgs/ElectricalFuse.svg";

export default function ElectricalFuse() {
  return <ElectricalFuseSVG className="itemImage" />;
}

import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../app/store";

export enum GameStatus {
  start = "start",
  intro = "intro",
  outro = "outro",
  playing = "playing",
}

interface GameState {
  status: GameStatus;
}

const initialState: GameState = { status: GameStatus.start };

const gameSlice = createSlice({
  name: "game",
  initialState,
  reducers: {
    setGameStatus(state, action: PayloadAction<GameStatus>) {
      state.status = action.payload;
    },
    resetGame(state) { },
  },
});

export const { setGameStatus, resetGame } = gameSlice.actions;

export const getGameStatus = (state: RootState) => state.game.status;

export default gameSlice.reducer;

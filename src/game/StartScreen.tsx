import { useAppDispatch } from "../app/hooks";
import { useEffect } from "react";
import { GameStatus, setGameStatus } from "./gameSlice";
import "./StartScreen.css"

export default function IntroductionScreen() {
  const dispatch = useAppDispatch();

  function onStartGameClick() {
    dispatch(setGameStatus(GameStatus.intro));
  }

  var openTab = (tabName: string) => {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent") as HTMLCollectionOf<HTMLElement>;
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = "tablinks";
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName)!.style.display = "block";
    document.getElementById(tabName + "Button")!.className += " active";
  }

  useEffect(() => { openTab("Team") })

  return (
    <section>
      <header className="App-header">
        <h1>Welcome to Escapucina !</h1>
        <img src="./maison_red_car.jpg" className="coverPageImage" alt=""></img>
      </header>

      <button className="startButton" onClick={onStartGameClick}>Démarrer l'aventure...</button>

      <div className="tabs">
        <div className="buttons">
          <button className="tablinks" id="TeamButton" onClick={() => openTab('Team')}>L'équipe</button>
          <button className="tablinks" id="ThanksButton" onClick={() => openTab('Thanks')}>Remerciements</button>
          <button className="tablinks" id="SorryButton" onClick={() => openTab('Sorry')}>Excuses</button>
        </div>

        <div id="Team" className="tabcontent">
          <h3>Par Ordre alphabétique d'utilité	</h3>
          <li>Rom-1		Technique</li>
          <li>Tiph-1		Consulting</li>
          <li>Mart-1		Design</li>
          <li>Gui-1		Enigmes</li>
          <br />
        </div>

        <div id="Thanks" className="tabcontent">
          <p>
            Est-ce vraiment notre genre ? <br />
            Non sérieusement, ce jeu est l'aboutissement d'une éducation
            ouverte et irréverencieuse, dans une maison qui l'est tout
            autant. <br />
            Alors, probablement, merci à tous ceux qui y ont particité de
            près ou de loin.
          </p>
        </div>

        <div id="Sorry" className="tabcontent">
          <p>
            Pas notre genre non plus.<br />
            Mais bon, si dans notre désir de remettre toute cette histoire
            en lumière, il est arrivé qu'on froisse un nerf, ou un égo,
            nous présentons par avance des excuses moyennement sincères.<br />
            Un peu d'humour, merde.
          </p>
        </div>
      </div>

    </section>
  );
}

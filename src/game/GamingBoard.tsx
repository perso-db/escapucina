import "./GamingBoard.css";
import CurrentRoom from "../rooms/CurrentRoom";
import Map from "../map/Map";
import Inventory from "../inventory/Inventory";
import Narration from "../narration/Narration";
import ItemOpened from "../items/ItemOpened";

export default function GamingBoard() {
  return (
    <div className="Board">
      <ItemOpened />
      <CurrentRoom />
      <Inventory />
      <Narration />
      <Map />
    </div>
  );
}

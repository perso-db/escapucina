import { useState } from "react";
import { useAppDispatch } from "../app/hooks";
import { GameStatus, setGameStatus } from "./gameSlice";
import "./IntroductionSequence.css";

export default function IntroductionSequence() {
  const dispatch = useAppDispatch();
  const [index, setIndex] = useState(1);

  const texts = [
    "Damneud ! La voiture s'éteint sur un dernier toussotement. Il fait noir et il neige...",
    "Une seule maison aux alentours. Quelques fenetres semblent allumées. Une silhouete se détache.",
    "La porte est ouverte, j'entre ? Il doit bien y avoir quelqu'un à l'intérieur.",
    "BLAM ! Un courant d'air vient de claquer la porte... Pas de poignée de ce côté-ci. Et M... Damneud ! "
  ]

  function next() {
    if (index < 4) { setIndex(index + 1) } else {
      dispatch(setGameStatus(GameStatus.playing))
    };
  }

  function renderSequence(index: number) {
    var imgUrl = "./IntroData/" + String(index) + ".jpg";
    return <div>
      <img src={imgUrl} alt={String(index)} className="imgSequence" height="500" />
      <br />
      <p className="textSequence" >{texts[index - 1]} </p>
    </div>
  }

  return (
    <section>
      <header className="App-header">
        <h1>Un soir d'été, dans un pays lointain</h1>
      </header>
      {renderSequence(index)}
      <br />
      <button className="startButton" onClick={next}>Continuer</button>
    </section>
  );
}

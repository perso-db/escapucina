import { useAppDispatch } from "../app/hooks";
import { GameStatus, setGameStatus, resetGame } from "./gameSlice";

export default function VictoryScreen() {
  const dispatch = useAppDispatch();

  function onRestartGameClick() {
    dispatch(resetGame());
    dispatch(setGameStatus(GameStatus.playing));
  }

  return (
    <section>
      <header className="App-header">
        <h1>Gagné !!! Bravo Doudou !!!</h1>
      </header>
      <button onClick={onRestartGameClick}>Recommencer</button>
    </section>
  );
}

import { useEffect } from "react";
import { useAppSelector } from "../app/hooks";
import { RootState } from "../app/store";
import RoomId from "../rooms/roomId";
import { ReactComponent as GroundFloorSVG } from "./GroundFloorSmall.svg";
import { ReactComponent as FirstFloorSVG } from "./FirstFloorSVG.svg";
import "./Map.css";

function Map() {
  const currentRoomId = useAppSelector(
    (state: RootState) => state.rooms.currentRoomId
  );

  type RoomsDict = {
    [key in RoomId]: JSX.Element;
  };

  const floorsPerRoom: RoomsDict = {
    // RDC New Names
    [RoomId.HallFaceUp]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.HallFaceDown]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.LivingRoomFaceUp]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.LivingRoomFaceRight]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.PoolFaceLeft]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.PoolFaceDown]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.StairsFaceUp]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.KitchenFaceLeft]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.KitchenFaceDown]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.Drawers]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.Frigogidaire]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.DiningRoomFaceUp]: <GroundFloorSVG className="Map" id="Map" />,

    //First Floor New Names
    [RoomId.FirstFloorFaceDown]: <FirstFloorSVG className="Map" id="Map" />,
    [RoomId.RoomRom1]: <FirstFloorSVG className="Map" id="Map" />,
    [RoomId.RoomTiph1]: <FirstFloorSVG className="Map" id="Map" />,
    [RoomId.RoomMart1]: <FirstFloorSVG className="Map" id="Map" />,
    [RoomId.RoomGui1]: <FirstFloorSVG className="Map" id="Map" />,

    //Mini-games
    [RoomId.ElectricalPanel]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.Bandelettes]: <GroundFloorSVG className="Map" id="Map" />,
    [RoomId.Lock]: <GroundFloorSVG className="Map" id="Map" />,
  };

  const roomElementId = () => {
    const elementIdLength = currentRoomId.indexOf("Face");
    if (elementIdLength > 0) {
      return currentRoomId.substring(0, elementIdLength);
    } else if (currentRoomId === "Drawers" || currentRoomId === "Frigogidaire") {
      return "Kitchen"
    } else if (currentRoomId === "Bandelettes") {
      return "Pool"
    } else if (currentRoomId === "Lock") {
      return "LivingRoom"
    }
    else {
      return currentRoomId;
    }
  };

  const reinitMap = () => {
    const elementMapLayer = document.getElementById("MapLayer");
    if (elementMapLayer) {
      const children = elementMapLayer.children;
      for (var i = 0; i < children.length; i++) {
        var child = children[i] as HTMLElement;
        child.classList.remove("MapCurrentRoom");
      }
    }
  };

  useEffect(() => {
    reinitMap();
    var element = document.getElementById(roomElementId());
    if (element) {
      element.classList.add("MapCurrentRoom");
    }
  });

  return floorsPerRoom[currentRoomId];
}

export default Map;

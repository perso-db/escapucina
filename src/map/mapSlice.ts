import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export enum Floors {
  firstFloor = "firstFloor",
  secondFloor = "secondFloor",
}

interface mapState {
  isMapOpened: boolean;
  floor: Floors;
}

const initialState: mapState = {
  isMapOpened: false,
  floor: Floors.firstFloor,
};

const mapSlice = createSlice({
  name: "map",
  initialState,
  reducers: {
    openMap(state) {
      state.isMapOpened = true;
    },
    closeMap(state) {
      state.isMapOpened = false;
    },
    setFloor(state, action: PayloadAction<Floors>) {
      state.floor = action.payload;
    },
  },
});

export const { openMap, closeMap, setFloor } = mapSlice.actions;

export default mapSlice.reducer;

Installer InkScape

## Nouvelle pièce

-   Fichier / Importer : sélectionner la photo
-   Normalement la photo est déjà sélectionnée. Sinon, cliquer dessus pour la sélectionner
-   Edition / Ajuster la taille de page à la sélection

## Créer une zone cliquable : option 1

-   Créer un objet de la forme voulue avec "Créer des rectantles" ou "Créer des cercles"
-   Avec l'object sélectionné (cliquer dessus si besoin) : Chemin / Objet en chemin
-   Click droit sur l'objet : Propriétés de l'objet
-   Remplir le champ ID avec une valeur ayant du sens (et ne se terminant pas par un chiffre)
-   Cliquer sur "Définir"

## Créer une zone cliquable : option 2

-   Créer un objet de la forme voulue avec "Tracer des courbes de Béziers"
-   Click droit sur l'objet : Propriétés de l'objet
-   Remplir le champ ID avec une valeur ayant du sens (et ne se terminant pas par un chiffre)
-   Cliquer sur "Définir"

## Enregistrement du fichier (à chaque fois, pas que la première fois)

-   Fichier / Enregistrer Sous (attention à ne pas faire un CRTL + S par erreur d'ailleurs)
-   Sélectionner SVG optimisé (les autres formats ne sont pas gérés par React) et choisir le nom de fichier
-   Sur l'écran suivant, vérifier que la case "Supprimer les ID inutilisés" est bien décochée

## Pour les objets

C'est tout pareil :)
